package com.roadmapper.web;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.roadmapper.db.DataAccess;
import com.roadmapper.dto.MapColl;

public class RoadMapperServlet extends HttpServlet { 
  protected void doGet(HttpServletRequest request, 
      HttpServletResponse response) throws ServletException, IOException 
  {
  	PrintWriter out = response.getWriter();
    try {
	    DataAccess dAccess = new DataAccess();
	    System.out.println("Before calling readdatabase");
	    MapColl mapColl = dAccess.readDataBase();
	    Gson gson = new Gson();
	    String json = "";
	    json = gson.toJson(mapColl);
	    out.println(json);
    } catch (Exception e) {
    	out.println(e.getMessage());
    }
    out.flush();
  }  
}