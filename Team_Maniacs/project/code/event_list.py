# -*- coding: utf-8 -*-
"""
Created on Sat Aug 26 11:05:47 2017

@author: u29673
"""

import pandas

def road_classifier(row):
    if (row['g'] > .7) & (row['g'] < 1.2):
        return "bad"
    if (row['g'] > 1.2) :
        return "ugly"
    return "good"


df = pandas.read_csv('C:\\Users\\u29673\\Documents\\project\\data\\training\\training_data.csv');
#serie = df.iloc[0,:]
#print (serie)
std_dev = df.iloc[:,1:4].rolling(40).std()
#print (std_dev)
df.iloc[:,1:4].plot()
std_dev.plot()
#new = pandas.concat([df,std_dev],axis=1)
df = df.assign(g=std_dev.iloc[:,2])

df['h'] =  df.apply(road_classifier,axis=1)
print (df)
df.to_csv('output.csv')

json = df.loc[(df['h'].isin(['bad','ugly','good']))]
f = open('GeoJSONP.js', 'w')
f.write('eqfeed_callback(\n{"type":"FeatureCollection","features":[\n')
for index,row in json.iterrows():
    f.write('{"type":"Feature","grade":"'+row['h']+'","geometry":{"type":"Point","coordinates":['+str(row['lo'])+','+str(row['la'])+']'+'},"id":"usc000csx3"},\n')
f.write(']}\n);')
f.close()