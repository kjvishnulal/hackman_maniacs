package com.roadmapper.dto;

import java.util.ArrayList;
import java.util.List;

public class MapColl {
	String type;
	List<MapDTO> features = new ArrayList<MapDTO> ();
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<MapDTO> getFeatures() {
		return features;
	}
	public void setFeatures(List<MapDTO> features) {
		this.features = features;
	}
}
