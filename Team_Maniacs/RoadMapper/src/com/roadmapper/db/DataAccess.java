package com.roadmapper.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.roadmapper.dto.Geometry;
import com.roadmapper.dto.MapColl;
import com.roadmapper.dto.MapDTO;

public class DataAccess {
    private Connection connect = null;
    private Statement statement = null;
    private PreparedStatement preparedStatement = null;
    private ResultSet resultSet = null;

    public MapColl readDataBase() throws Exception {
        try {
            // This will load the MySQL driver, each DB has its own driver
            Class.forName("com.mysql.jdbc.Driver");
            // Setup the connection with the DB
            connect = DriverManager
                    .getConnection("jdbc:mysql://localhost:3306/phmapperdb?"
                            + "user=phmapper&password=PhM4pper");

            // Statements allow to issue SQL queries to the database
            System.out.println("Before connecting to database");
            statement = connect.createStatement();
            // Result set get the result of the SQL query
            resultSet = statement
                    .executeQuery("select * from map_info");
            return writeResultSet(resultSet);

            /*// PreparedStatements can use variables and are more efficient
            preparedStatement = connect
                    .prepareStatement("insert into  user_data values (default, ?, ?, ?, ? , ?, ?)");
            // "myuser, webpage, datum, summary, user_data from user_data");
            // Parameters start with 1
            preparedStatement.setString(1, "Test");
            preparedStatement.setString(2, "TestEmail");
            preparedStatement.setString(3, "TestWebpage");
            preparedStatement.setDate(4, new java.sql.Date(2009, 12, 11));
            preparedStatement.setString(5, "TestSummary");
            preparedStatement.setString(6, "TestComment");
            preparedStatement.executeUpdate();

            preparedStatement = connect
                    .prepareStatement("SELECT myuser, webpage, datum, summary, user_data from user_data");
            resultSet = preparedStatement.executeQuery();
            writeResultSet(resultSet);

            // Remove again the insert comment
            preparedStatement = connect
            .prepareStatement("delete from user_data where myuser= ? ; ");
            preparedStatement.setString(1, "Test");
            preparedStatement.executeUpdate();

            resultSet = statement
            .executeQuery("select * from user_data");
            writeMetaData(resultSet);*/

        } catch (Exception e) {
            throw e;
        } finally {
            close();
        }

    }

    private void writeMetaData(ResultSet resultSet) throws SQLException {
        //  Now get some metadata from the database
        // Result set get the result of the SQL query

        System.out.println("The columns in the table are: ");

        System.out.println("Table: " + resultSet.getMetaData().getTableName(1));
        for  (int i = 1; i<= resultSet.getMetaData().getColumnCount(); i++){
            System.out.println("Column " +i  + " "+ resultSet.getMetaData().getColumnName(i));
        }
    }

    private MapColl writeResultSet(ResultSet resultSet) throws SQLException {
        // ResultSet is initially before the first data set
    	MapColl mapColl = new MapColl();
    	mapColl.setType("FeatureCollection");
    	List<MapDTO> mapCoords = new ArrayList<MapDTO> ();
        while (resultSet.next()) {
            // It is possible to get the columns via name
            // also possible to get the columns via the column number
            // which starts at 1
            // e.g. resultSet.getSTring(2);
        	
        	MapDTO mapDto = new MapDTO();
        	mapDto.setType("Feature");
        	mapDto.setGrade(resultSet.getString("road_condition"));
        	mapDto.setId("usc000csx3");
        	Geometry geometry = new Geometry();
        	geometry.setType("Point");
        	geometry.setCoords(resultSet.getDouble("latitude") + "," + resultSet.getDouble("longitude"));
        	mapDto.setGeometry(geometry);
        	mapColl.getFeatures().add(mapDto);
        }
        return mapColl;
    }

    // You need to close the resultSet
    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }

            if (statement != null) {
                statement.close();
            }

            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

}
